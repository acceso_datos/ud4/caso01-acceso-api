package accesoapi;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import com.google.gson.Gson;

// https://mkyong.com/webservices/jax-rs/restfull-java-client-with-java-net-url/
public class metodoGET {
	public static void main(String[] args) {

		System.out.println("----------- PIDE UN CLIENTE ---------------");
		try {
			Cliente cliente = dameCliente(1);
			System.out.println(cliente);
		} catch (RuntimeException e) {
			System.out.println(e.getMessage());
		}

		System.out.println("----------- PIDE TODOS LOS CLIENTES ---------------");
		try {
			List<Cliente> clientes = dameClientes();
			clientes.stream().forEach(System.out::println);
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}

	}

	private static Cliente dameCliente(int id) {
		Gson gson;		
		HttpURLConnection con = null;
		try {
			URL url = new URL("http://localhost:8080/clientes/" + id);
			con = (HttpURLConnection) url.openConnection();
			con.setRequestMethod("GET");
			con.setRequestProperty("Accept", "application/json");

			if (con.getResponseCode() != HttpURLConnection.HTTP_OK) {
				// System.out.println("Fallo leyendo recurso. Código de error: " +
				// con.getResponseCode());
				throw new RuntimeException("Failed : HTTP error code : " + con.getResponseCode());
			} else {
				BufferedReader br = new BufferedReader(new InputStreamReader((con.getInputStream())));
				String lin, respuesta = "";
				while ((lin = br.readLine()) != null) {
					respuesta = respuesta.concat(lin);
				}
				gson = new Gson();
				return gson.fromJson(respuesta, Cliente.class);
			}
		} catch (IOException e) {
			System.out.println(e.getMessage());
			return null;
		} finally {
			if (con != null) {
				con.disconnect();
			}
		}
	}

	private static List<Cliente> dameClientes() {
		Gson gson;
		HttpURLConnection con = null;
		try {
			URL url = new URL("http://localhost:8080/clientes");
			con = (HttpURLConnection) url.openConnection();
			con.setRequestMethod("GET");
			con.setRequestProperty("Accept", "application/json");

			if (con.getResponseCode() != HttpURLConnection.HTTP_OK) {
				// System.out.println("Fallo leyendo recurso. Código de error: " +
				// con.getResponseCode());
				throw new RuntimeException("Failed : HTTP error code : " + con.getResponseCode());
			} else {
				BufferedReader br = new BufferedReader(new InputStreamReader((con.getInputStream())));
				String lin, respuesta = "";
				while ((lin = br.readLine()) != null) {
					respuesta = respuesta.concat(lin);
				}

				gson = new Gson();
				Cliente[] clientes = gson.fromJson(respuesta, Cliente[].class);
				return Arrays.asList(clientes);
			}
		} catch (IOException e) {
			System.out.println(e.getMessage());
			return new ArrayList<Cliente>();
		} finally {
			if (con != null) {
				con.disconnect();
			}			
		}
	}
}
