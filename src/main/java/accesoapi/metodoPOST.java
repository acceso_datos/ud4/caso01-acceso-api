package accesoapi;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;

import com.google.gson.Gson;


public class metodoPOST {
	public static void main(String[] args) {
		Gson gson;
		try {
			gson = new Gson();
			Cliente cliente = new Cliente("Nuevo cli", "direccion nuevo cli, 12");
			String clienteJson = gson.toJson(cliente, Cliente.class);
			enviaCliente(clienteJson);
			System.out.println("¡Creado!");
		} catch (RuntimeException e) {
			System.out.println(e.getMessage());
		}
		

	}

	private static void enviaCliente(String clienteJson) {	

		HttpURLConnection con = null;
		try {
			URL url = new URL("http://localhost:8080/clientes");
			con = (HttpURLConnection) url.openConnection();
			con.setDoOutput(true);
			con.setRequestMethod("POST");
			con.setRequestProperty("Content-Type", "application/json");

			OutputStream os = con.getOutputStream();
			os.write(clienteJson.getBytes());
			os.flush();
			
			if (con.getResponseCode() != HttpURLConnection.HTTP_CREATED) {
				throw new RuntimeException("Failed : HTTP error code : " + con.getResponseCode());
			} 
			
			// lectura del cuerpo de la respuesta
			String respuesta;
			BufferedReader br = new BufferedReader(new InputStreamReader((con.getInputStream())));
			respuesta = br.readLine();
			int idCreado = Integer.parseInt(respuesta);
			System.out.println(idCreado);	
			
		} catch (IOException e) {
			System.out.println(e.getMessage());
		} finally {
			if (con != null) {
				con.disconnect();
			}
		}
	}

	
}
